SHELL=/bin/sh

## Colors
ifndef VERBOSE
.SILENT:
endif
COLOR_COMMENT=\033[0;32m
IMAGE_PATH=/betd/public/docker/jenkins-dind
REGISTRY_DOMAIN=registry.gitlab.com
VERSION=latest
IMAGE=${REGISTRY_DOMAIN}${IMAGE_PATH}:${VERSION}

## Help
help:
	printf "${COLOR_COMMENT}Usage:${COLOR_RESET}\n"
	printf " make [target]\n\n"
	printf "${COLOR_COMMENT}Available targets:${COLOR_RESET}\n"
	awk '/^[a-zA-Z\-\_0-9\.@]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf " ${COLOR_INFO}%-16s${COLOR_RESET} %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

## build and push image
all: build push_image

## build image and tags it
build: Dockerfile
	docker build -f Dockerfile . -t ${IMAGE}

## login on registry
registry_login:
ifeq ($(CI_BUILD_TOKEN),)
	docker login ${REGISTRY_DOMAIN}
else
	printf "Docker login from CI with ${CI_REGISTRY_USER} to ${CI_REGISTRY}\n"
	docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
endif

## pull image
pull_image: registry_login
	docker pull ${IMAGE}

## push image
push_image: registry_login
	docker push ${IMAGE}
